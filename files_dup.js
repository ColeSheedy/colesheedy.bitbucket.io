var files_dup =
[
    [ "button_led.py", "button__led_8py.html", "button__led_8py" ],
    [ "controllerDriverW3.py", "controllerDriverW3_8py.html", [
      [ "controller", "classcontrollerDriverW3_1_1controller.html", "classcontrollerDriverW3_1_1controller" ]
    ] ],
    [ "controllerDriverW4.py", "controllerDriverW4_8py.html", [
      [ "controller", "classcontrollerDriverW4_1_1controller.html", "classcontrollerDriverW4_1_1controller" ]
    ] ],
    [ "controllerTaskW3.py", "controllerTaskW3_8py.html", [
      [ "controllerTask", "classcontrollerTaskW3_1_1controllerTask.html", "classcontrollerTaskW3_1_1controllerTask" ]
    ] ],
    [ "controllerTaskW4.py", "controllerTaskW4_8py.html", [
      [ "controllerTask", "classcontrollerTaskW4_1_1controllerTask.html", "classcontrollerTaskW4_1_1controllerTask" ]
    ] ],
    [ "dataHandlingClassW1.py", "dataHandlingClassW1_8py.html", [
      [ "dataHandling", "classdataHandlingClassW1_1_1dataHandling.html", "classdataHandlingClassW1_1_1dataHandling" ]
    ] ],
    [ "elevator_simulation.py", "elevator__simulation_8py.html", "elevator__simulation_8py" ],
    [ "encoderDriverW2.py", "encoderDriverW2_8py.html", [
      [ "encoder", "classencoderDriverW2_1_1encoder.html", "classencoderDriverW2_1_1encoder" ]
    ] ],
    [ "encoderDriverW3.py", "encoderDriverW3_8py.html", [
      [ "encoder", "classencoderDriverW3_1_1encoder.html", "classencoderDriverW3_1_1encoder" ]
    ] ],
    [ "encoderDriverW4.py", "encoderDriverW4_8py.html", [
      [ "encoder", "classencoderDriverW4_1_1encoder.html", "classencoderDriverW4_1_1encoder" ]
    ] ],
    [ "encoderTaskW2.py", "encoderTaskW2_8py.html", [
      [ "encoderTask", "classencoderTaskW2_1_1encoderTask.html", "classencoderTaskW2_1_1encoderTask" ]
    ] ],
    [ "fibonacci.py", "fibonacci_8py.html", "fibonacci_8py" ],
    [ "mainW1.py", "mainW1_8py.html", "mainW1_8py" ],
    [ "mainW2.py", "mainW2_8py.html", "mainW2_8py" ],
    [ "mainW3.py", "mainW3_8py.html", "mainW3_8py" ],
    [ "mainW4.py", "mainW4_8py.html", "mainW4_8py" ],
    [ "morse_code.py", "morse__code_8py.html", "morse__code_8py" ],
    [ "motorDriverW3.py", "motorDriverW3_8py.html", [
      [ "motor", "classmotorDriverW3_1_1motor.html", "classmotorDriverW3_1_1motor" ]
    ] ],
    [ "motorDriverW4.py", "motorDriverW4_8py.html", [
      [ "motor", "classmotorDriverW4_1_1motor.html", "classmotorDriverW4_1_1motor" ]
    ] ],
    [ "shareW2.py", "shareW2_8py.html", "shareW2_8py" ],
    [ "shareW3.py", "shareW3_8py.html", "shareW3_8py" ],
    [ "shareW4.py", "shareW4_8py.html", "shareW4_8py" ],
    [ "simon_says_game.py", "simon__says__game_8py.html", "simon__says__game_8py" ],
    [ "simon_says_game_class.py", "simon__says__game__class_8py.html", [
      [ "game", "classsimon__says__game__class_1_1game.html", "classsimon__says__game__class_1_1game" ]
    ] ],
    [ "UITaskW2.py", "UITaskW2_8py.html", [
      [ "UITask", "classUITaskW2_1_1UITask.html", "classUITaskW2_1_1UITask" ]
    ] ],
    [ "UITaskW3.py", "UITaskW3_8py.html", [
      [ "UITask", "classUITaskW3_1_1UITask.html", "classUITaskW3_1_1UITask" ]
    ] ],
    [ "UITaskW4.py", "UITaskW4_8py.html", [
      [ "UITask", "classUITaskW4_1_1UITask.html", "classUITaskW4_1_1UITask" ]
    ] ],
    [ "userInterfaceFrontW2.py", "userInterfaceFrontW2_8py.html", "userInterfaceFrontW2_8py" ],
    [ "userInterfaceFrontW3.py", "userInterfaceFrontW3_8py.html", "userInterfaceFrontW3_8py" ],
    [ "userInterfaceFrontW4.py", "userInterfaceFrontW4_8py.html", "userInterfaceFrontW4_8py" ],
    [ "userInterfaceW1.py", "userInterfaceW1_8py.html", "userInterfaceW1_8py" ]
];