var annotated_dup =
[
    [ "controllerDriverW3", null, [
      [ "controller", "classcontrollerDriverW3_1_1controller.html", "classcontrollerDriverW3_1_1controller" ]
    ] ],
    [ "controllerDriverW4", null, [
      [ "controller", "classcontrollerDriverW4_1_1controller.html", "classcontrollerDriverW4_1_1controller" ]
    ] ],
    [ "controllerTaskW3", null, [
      [ "controllerTask", "classcontrollerTaskW3_1_1controllerTask.html", "classcontrollerTaskW3_1_1controllerTask" ]
    ] ],
    [ "controllerTaskW4", null, [
      [ "controllerTask", "classcontrollerTaskW4_1_1controllerTask.html", "classcontrollerTaskW4_1_1controllerTask" ]
    ] ],
    [ "dataHandlingClassW1", null, [
      [ "dataHandling", "classdataHandlingClassW1_1_1dataHandling.html", "classdataHandlingClassW1_1_1dataHandling" ]
    ] ],
    [ "encoderDriverW2", null, [
      [ "encoder", "classencoderDriverW2_1_1encoder.html", "classencoderDriverW2_1_1encoder" ]
    ] ],
    [ "encoderDriverW3", null, [
      [ "encoder", "classencoderDriverW3_1_1encoder.html", "classencoderDriverW3_1_1encoder" ]
    ] ],
    [ "encoderDriverW4", null, [
      [ "encoder", "classencoderDriverW4_1_1encoder.html", "classencoderDriverW4_1_1encoder" ]
    ] ],
    [ "encoderTaskW2", null, [
      [ "encoderTask", "classencoderTaskW2_1_1encoderTask.html", "classencoderTaskW2_1_1encoderTask" ]
    ] ],
    [ "motorDriverW3", null, [
      [ "motor", "classmotorDriverW3_1_1motor.html", "classmotorDriverW3_1_1motor" ]
    ] ],
    [ "motorDriverW4", null, [
      [ "motor", "classmotorDriverW4_1_1motor.html", "classmotorDriverW4_1_1motor" ]
    ] ],
    [ "simon_says_game_class", null, [
      [ "game", "classsimon__says__game__class_1_1game.html", "classsimon__says__game__class_1_1game" ]
    ] ],
    [ "UITaskW2", null, [
      [ "UITask", "classUITaskW2_1_1UITask.html", "classUITaskW2_1_1UITask" ]
    ] ],
    [ "UITaskW3", null, [
      [ "UITask", "classUITaskW3_1_1UITask.html", "classUITaskW3_1_1UITask" ]
    ] ],
    [ "UITaskW4", null, [
      [ "UITask", "classUITaskW4_1_1UITask.html", "classUITaskW4_1_1UITask" ]
    ] ]
];