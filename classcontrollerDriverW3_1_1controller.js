var classcontrollerDriverW3_1_1controller =
[
    [ "__init__", "classcontrollerDriverW3_1_1controller.html#ab093e3a91934b40cdf28f67ca6b6ecd9", null ],
    [ "get_duty", "classcontrollerDriverW3_1_1controller.html#a746c73b065fd2a7a280e6de0d18b9201", null ],
    [ "get_Kp", "classcontrollerDriverW3_1_1controller.html#a4e5391deb0de5657c7a2136cd1bde0a6", null ],
    [ "run", "classcontrollerDriverW3_1_1controller.html#a0f6fb3a6108c05caa5bdcd9a9409d99c", null ],
    [ "set_Kp", "classcontrollerDriverW3_1_1controller.html#ad16a2859c68a7c5f826daf42b28f2f64", null ],
    [ "KpPrime", "classcontrollerDriverW3_1_1controller.html#a856999424e0eb5a7c51e0b310dd63c38", null ],
    [ "L", "classcontrollerDriverW3_1_1controller.html#a3808ee7843c7d75388d187725df68f3f", null ]
];