var index =
[
    [ "Lab 1: Fibonacci Number Calculator", "first.html", null ],
    [ "Lab 2: LED FLickering and Interacting with Button", "second.html", null ],
    [ "Lab 3: Morse Code Simon Says Game with Button Interaction", "third.html", null ],
    [ "Final Project W1: Decaying Sine Wave and Serial Communication", "fourth.html", null ],
    [ "Final Project W2: Encoder Implementation and Position Graphing", "fifth.html", null ],
    [ "Final Project W3: Closed Loop P-Controller, Motor Implementation, & Velocity Graphing", "sixth.html", null ],
    [ "Final Project W4: Closed Loop PI-Controller and Reference Tracking", "seventh.html", null ],
    [ "HW 2: Finite State Machine Practice - Elevator Simulation", "eighth.html", null ]
];