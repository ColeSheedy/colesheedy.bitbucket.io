var classmotorDriverW4_1_1motor =
[
    [ "__init__", "classmotorDriverW4_1_1motor.html#af6729962a9a06ce1adc5236decebd91c", null ],
    [ "disable", "classmotorDriverW4_1_1motor.html#abee0b999bb73b532522daedd2afbb64b", null ],
    [ "enable", "classmotorDriverW4_1_1motor.html#a2cc7de7a45bed8fcf11daa58f104b673", null ],
    [ "set_duty", "classmotorDriverW4_1_1motor.html#a324ddec60b9d855b45cdac927b8f3c8c", null ],
    [ "pin1", "classmotorDriverW4_1_1motor.html#abea89ece3fb015f6842417ce85040742", null ],
    [ "pin2", "classmotorDriverW4_1_1motor.html#a81d34bc067b9b3b55a590b79f8afadd1", null ],
    [ "sleepPin", "classmotorDriverW4_1_1motor.html#a79bf2a4597507b5ffde5363726327571", null ],
    [ "tim", "classmotorDriverW4_1_1motor.html#ad541fd89ac42bbcbe32f6387e141fef1", null ],
    [ "tim_ch1", "classmotorDriverW4_1_1motor.html#a308636a8007de9ab0c628031fb849761", null ],
    [ "tim_ch2", "classmotorDriverW4_1_1motor.html#a2cbb5f27f845069821f4f73378309725", null ]
];