var classcontrollerDriverW4_1_1controller =
[
    [ "__init__", "classcontrollerDriverW4_1_1controller.html#a339c2bfbe17f752fba47ce8fc3ec42ed", null ],
    [ "get_duty", "classcontrollerDriverW4_1_1controller.html#a107d6f6a353b65d53afb72bb1585c3ea", null ],
    [ "get_Ki", "classcontrollerDriverW4_1_1controller.html#a491f6a97b377eb09ece7bd1d179b55e8", null ],
    [ "get_Kp", "classcontrollerDriverW4_1_1controller.html#a8b5307dd8519c341fa6f944445798794", null ],
    [ "run", "classcontrollerDriverW4_1_1controller.html#a9f6ac9129c2508eb317b9057fdf3d17e", null ],
    [ "set_Ki", "classcontrollerDriverW4_1_1controller.html#ac03e85465b2bde97c0582747ad5228de", null ],
    [ "set_Kp", "classcontrollerDriverW4_1_1controller.html#a3647978f666e637e60c5aba6e1923e21", null ],
    [ "KiPrime", "classcontrollerDriverW4_1_1controller.html#a421db96d655724e46fd3ca206b05e4d2", null ],
    [ "KpPrime", "classcontrollerDriverW4_1_1controller.html#ae28289dfeab12c72142b610445dcdcb0", null ],
    [ "L", "classcontrollerDriverW4_1_1controller.html#a33af26d3cc08da96c54e49c945119456", null ]
];