var classmotorDriverW3_1_1motor =
[
    [ "__init__", "classmotorDriverW3_1_1motor.html#ac0da2a9aea7fbc76fdf2cd47c3c82c87", null ],
    [ "disable", "classmotorDriverW3_1_1motor.html#ae5bb27d11a7e83a4ead4be598124e72f", null ],
    [ "enable", "classmotorDriverW3_1_1motor.html#a74e6bad36b319dcc6578abfa37be1360", null ],
    [ "set_duty", "classmotorDriverW3_1_1motor.html#adefe496a7428b530d538a0b2fd810335", null ],
    [ "pin1", "classmotorDriverW3_1_1motor.html#a39c3ec792074b5329d8f62e48c326554", null ],
    [ "pin2", "classmotorDriverW3_1_1motor.html#addb6394c1de08d2a5ef999e6bf37cee4", null ],
    [ "sleepPin", "classmotorDriverW3_1_1motor.html#a489f8c326b30dbe55e03fc58a1261072", null ],
    [ "tim", "classmotorDriverW3_1_1motor.html#ae55491c7eacbb5b7d2e4d48c06dcbc78", null ],
    [ "tim_ch1", "classmotorDriverW3_1_1motor.html#af9c109b11f93fe852d850060f82d7bc6", null ],
    [ "tim_ch2", "classmotorDriverW3_1_1motor.html#af46d908d88702264f9464f98330b556f", null ]
];